# bme280 polling

this small script i wrote implements the polling and calibration,
as defined in the BME280 environment sensor's Data sheet, with python3


## Getting started
get a sensor, hook it up to your SPI pins
and update the address and port in the script.

## Usage
to test out if the sensor is connected and working
```bash
./bme280.py
```
if you like to install it as a webservice:
```bash
mkdir /opt/BME280
cp *.py /opt/BME280/
# with systemd:
cp startup/bme280.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable bme280
systemctl start bme280
# or open-rc:
cp startup/bme280 /etc/init.d/
rc-service bme280 start
rc-update add bme280 default
```

## Support
if you have any questions message me in Discord `Katana Steel#0843` or Mastodon `@Katana-Steel@mast.linuxgamecast.com`,
if you have suggestions or improvements feel free to create a merge request, or send me a patch

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
this script is licensed under the GPLv3 found in the repo as the LICENSE file

## Project status


