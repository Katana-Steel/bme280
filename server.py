#!/usr/bin/python3
from bme280 import get_temp
import http.server as webserver
import socketserver
port = 80


class Handler (webserver.SimpleHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200, 'ok')
        self.send_header('Content-Type', 'application/json')
        self.end_headers()
        resp = get_temp()
        self.wfile.write(resp.encode('utf8'))
        self.wfile.write(b'\n')


if __name__ == '__main__':
    httpd = socketserver.TCPServer(("", port), Handler)

    print("serving at port {}".format(port))
    httpd.serve_forever()
