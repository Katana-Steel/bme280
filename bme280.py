#!/usr/bin/python3
import spidev
import struct
import json
"""
0x72:
2:0 os_h oversampling hum
0x74:
7:5 os_t oversampling temp
4:2 os_p oversampling press
1:0 mode (11 normal, 00 sleep, other force)
0x75:
5:7 t_sb t_standby
"""


def get_spi_data(bus=0, dev=0):
    spi = spidev.SpiDev()
    spi.open(bus, dev)
    spi.xfer2([0x72, 3, 0x74, 0b01101111, 0x75, 0xe0])
    while 13 in spi.xfer([0xf3, 0])[1:]:
        pass
    raw = get_raws(spi)
    calibration = get_calibrations(spi)
    spi.close()
    degC, pa_8, hum = calculate(raw, calibration)
    return json.dumps({
        'temp': [degC/100.0, (degC * 1.8 + 3200)/100.0],
        'pressure': (pa_8/25600),
        'humid': (hum/1024)
    })


def calculate(raws, cali):
    rt, rp, rh = raws
    cal_t, cal_p, cal_h = cali
    # temperatur
    v1 = (((rt >> 3)-(cal_t[0] << 1)) * cal_t[1]) >> 11
    v2 = ((((rt >> 4) - cal_t[0])**2) >> 12)*cal_t[2] >> 14
    f = v1 + v2
    degC = (f*5+128) >> 8
    # pressure
    v1 = f - 128000
    v12 = v1**2
    v2 = v12 * cal_p[5]
    v2 = v2 + (v1 * cal_p[4] << 17)
    v2 = v2 + (cal_p[3] << 35)
    v1 = (v12 * cal_p[2] >> 8) + (v1 * cal_p[1] << 12)
    v1 = (((1 << 47)+v1) * cal_p[0]) >> 33
    pa_8 = 0
    if v1 != 0:
        pa_8 = (1 << 20) - rp
        pa_8 = int((((pa_8 << 31)-v2) * 3125) / v1)
        v1 = (cal_p[8] * (pa_8 >> 13)**2) >> 25
        v2 = (cal_p[7] * pa_8) >> 19
        pa_8 = ((pa_8 + v1 + v2) >> 8) + (cal_p[6] << 4)
    # humitity
    v1 = f - 76800
    v1 = (((((rh << 14) - (cal_h[3] << 20) - (cal_h[4] * v1)) + (1 << 14)) >> 15) * (
        (((((v1 * cal_h[5]) >> 10) * (((v1 * cal_h[2]) >> 11) + (1 << 15))) >> 10) + (1 << 21)) * cal_h[1] + (1 << 13)) >> 14)
    v_15_2 = (v1 >> 15)**2
    v1 -= ((v_15_2 >> 7) * cal_h[0]) >> 4
    hum = max(0, min(v1, 25 << 24)) >> 12
    return (degC, pa_8, hum)


def comb(i):
    return (i[0] << 12 | i[1] << 4 | i[2] >> 4)


def get_raws(spi):
    raw = spi.xfer([0xf7]+8*[0])[1:]
    raw_hum = raw[-2] << 8 | raw[-1]
    raw_temp = comb(raw[3:6])
    raw_press = comb(raw[:3])
    return (raw_temp, raw_press, raw_hum)


def get_calibrations(spi):
    cal_t = struct.unpack('Hhh', struct.pack(
        'BBBBBB', *(spi.xfer([0x88]+6*[0])[1:])))
    cal_p = struct.unpack('Hhhhhhhhh', struct.pack(
        'BB'*9, *(spi.xfer([0x8e]+18*[0])[1:])))
    cal_h = []
    cal_h.extend(spi.xfer([0xa1, 0])[1:])
    cal_h.extend(struct.unpack('h', struct.pack(
        'BB', *(spi.xfer([0xe1, 0, 0])[1:]))))
    cal_h.extend(spi.xfer([0xa3, 0])[1:])
    th = spi.xfer([0xe4, 0, 0, 0, 0])[1:]
    cal_h.append((th[0] << 4) | (0x0f & th[1]))
    cal_h.append(((th[1] & 0xf0) >> 4) | (th[2] << 4))
    cal_h.append(th[3])
    return (cal_t, cal_p, cal_h)


def get_temp(bus=0, dev=0):
    return get_spi_data(bus, dev)


if __name__ == '__main__':
    print(get_spi_data())
